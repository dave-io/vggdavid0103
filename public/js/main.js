

 function readFromLocalStorage(){ // This function returns Array or false
  let jobStorage = localStorage.getItem('jobStorage'); // [] or undefined  |||||  FALSY VALUES ===> 0 undefined, null, '', false.
  if(jobStorage){ // Check if jobStorage exists in local storage, if undefined or falsy, do not execute the following lines of code
      let parsedJobStorage = JSON.parse(jobStorage); // Always JSON.parse() data coming from the localstorage if you are expecting an Array                                                // or Object
      if(Array.isArray(parsedJobStorage)){ // Check if data is an array. If true, return data
          return parsedJobStorage;
      }else{ // If false, return false;
          return false
      }
  }else{
      return false
  }
}

function renderToDom (){
  let jobs = readFromLocalStorage(); // An Array or false
  let lists = '';

  if(jobs){
    for( let i = 0 ; i < jobs.length ; i++){
        let currentJob = jobs[i];
        lists += '<li class="jobAdded";><p>' + currentJob.title + "</p><p>" + currentJob.location + "</p><p>" + currentJob.desc;
        lists += "</p><p>" + currentJob.expires + "</p></li>" ;
    }
  }else{
    lists += "<li style=''><p>You have not created any Jobs yet</p></li>";
  }
  
  document.querySelector('#jobListing').innerHTML = lists; 
}
     

