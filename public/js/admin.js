
function getFormData(){
    let title = document.querySelector('#title').value;
    let desc = document.querySelector('#desc').value;
    let department = document.querySelector('#department').value;
    let location = document.querySelector('#location').value;
    let expires = document.querySelector('#expires').value;


    let formData = {
        id: new Date().getTime() + Math.floor(Math.random() + 1),
        title: title,
        desc: desc,
        department: department,
        location: location,
        expires: expires
    }

    console.log(formData);
    return formData;
}

function submitForm(){
    let formData = getFormData();
    console.log(formData);
    saveToLocalStorage(formData);
    renderToDom (); //render to the dom
}

function saveToLocalStorage(formObject){
    let jobStorage = localStorage.getItem('jobStorage'); //[] or undefined  |||||  FALSY VALUES ===> 0, undefined, null, '', false.
    if(jobStorage){
        let parsedJobStorage = JSON.parse(jobStorage);
        if(Array.isArray(parsedJobStorage)){
            parsedJobStorage.push(formObject);
            localStorage.setItem('jobStorage', JSON.stringify(parsedJobStorage));
        }else{
            return false;
        }
    }else{
        let newJobStorage = [];
        newJobStorage.push(formObject);
        localStorage.setItem('jobStorage', JSON.stringify( newJobStorage));
    }
} 
readFromLocalStorage();
renderToDom();

