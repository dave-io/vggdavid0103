// const url = 'https://jsonplaceholder.typicode.com/comments?results=5';

/*
Promise.all(urls.map(url =>{
    return fetch(url).then(resp => resp.json())
})).then(results => {
    console.log(results[0]);
    // console.log(results[1]);
    
}).catch(() => console.log('Error!'))

*/



let ul = document.getElementById('comments');
let url = 'https://jsonplaceholder.typicode.com/posts?_start0&_limit=20';

fetch(url)
    .then(function (resp) {
        return resp.json();
    })
    .then(function (data) {
        appendData(data);
        console.log(data)
    })
    .catch(function (error) {
        console.log(error);
    });

function appendData(data) {
    let commentsContainer = document.getElementById('recentPosts');
    let output= " ";
    for (let i = 0; i < data.length; i++) {
       output += `<div class="recent box">
       <img class="recentImg" src="../images/laptop-3087585_640.jpg">
       <a class="title" href="./../www/comments.html?id=${data[i].id}">${data[i].title}</a>
       <p>${data[i].body}</p></div>`

       commentsContainer.innerHTML = output;
    }

}


function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  let id = getUrlParameter('id');
  let url2 = `https://jsonplaceholder.typicode.com/comments?postId=${id}`;

  fetch(url2)
    .then(function (resp) {
        return resp.json();
    })
    .then(function (data) {
        appendData2(data);
    })
    .catch(function (error) {
    });

function appendData2(data) {
    let output= "";
    for (let i = 0; i < data.length; i++) {
       output += `<div class="recent commentLine">
       <p class="title">${data[i].name}</p>${data[i].email} 
       <p>${data[i].body}</p></div>`

       ul.innerHTML = output;
    }

}


let current_page = 1;
let records_per_page = 5;

var objJson = output;

function prevPage()
{
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

function nextPage()
{
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}
    
function changePage(page)
{
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    var listing_table = document.getElementById("listingTable");
    var page_span = document.getElementById("page");
 
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (var i = (page-1) * records_per_page; i < (page * records_per_page) && i < data.length; i++) {
        listing_table.innerHTML += data[i].name + "<br>";
    }
    page_span.innerHTML = page + "/" + numPages();

    if (page == 1) {
        btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.style.visibility = "visible";
    }

    if (page == numPages()) {
        btn_next.style.visibility = "hidden";
    } else {
        btn_next.style.visibility = "visible";
    }
}

function numPages()
{
    return Math.ceil(data.length / records_per_page);
}

window.onload = function() {
    changePage(1);
};



