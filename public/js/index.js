
function createjobItemComponent(job){
    let jobItem = '<div class="job-item">';
    jobItem += '<a class="details">';
    jobItem += '<div class="role details">';
    jobItem += '<h5 title="' + job.title + '" id ="' + job.id +'">' + job.title + '</h5>';
    jobItem += '</div>' + '</a>';
    job.desc + '</span></div>';
    jobItem += '<div title="' + job.department + '" class="department jobAddOn details"><span>' + job.department + '</span></div>';
    jobItem += '<div title="' + job.location + '" class="location details"><i class="fas fa-map-marker-alt pr-2"></i><span>' + job.location + '</span></div>' + '</div>';
    // jobItem += '<div><a class="details" name ="' + job.id +'">View</a></div>' + '</div>'
     

    return jobItem;
}

// const jobDetails = document.querySelector(".location");
// console.log(jobDetails);

// function test() {
//   if(localStorage.getItem('jobStorage') !== null){
//     document.querySelector('.job-item').addEventListener("click", function name(e) {
//       alert('yes')
//     });
//     }else return;
// }
// test();


function renderjobsToDom(filter='all'){
    let jobsFromLocalStorage = readFromLocalStorage();
    // console.log(filter);
    
    if(jobsFromLocalStorage){
      let jobComponents = '';
      if(filter === 'all'){
        for(let i = 0; i < jobsFromLocalStorage.length ; i++ ){
          let currentJob = jobsFromLocalStorage[i];

          let jobComponent = createjobItemComponent(currentJob);
          jobComponents += jobComponent;
        }

        document.querySelector('#jobListings').innerHTML = jobComponents;
        document.querySelector('#no-result').style.display = 'block';
        document.querySelector('#no-result').innerHTML =  jobsFromLocalStorage.length + ' jobs found';
        return;

      }else{
        let filteredJobs = [];

        for(let i = 0; i < jobsFromLocalStorage.length ; i++ ){
          let currentJob = jobsFromLocalStorage[i];
          if(currentJob.hasOwnProperty('department')){
            if(currentJob.department.toLowerCase().includes(filter.toLowerCase())){
              filteredJobs.push(currentJob);
            }
          }
        }

        if(filteredJobs.length > 0){

          for(let i = 0; i < filteredJobs.length ; i++ ){
              let currentJob = filteredJobs[i];

              let jobComponent = createjobItemComponent(currentJob);
              jobComponents += jobComponent;
          }

          document.querySelector('#jobListings').innerHTML = jobComponents;
          document.querySelector('#no-result').style.display = 'block';
          document.querySelector('#no-result').innerHTML =  filteredJobs.length + ' jobs found';
        }else{
          document.querySelector('#jobListings').innerHTML = '';
          document.querySelector('#no-result').style.display = 'block';
          document.querySelector('#no-result').innerHTML = '0 jobs found';
        }
      }
    }else{
        document.querySelector('#jobListings').innerHTML = '';
        document.querySelector('#no-result').style.display = 'block';
        document.querySelector('#no-result').innerHTML = '0 jobs found';
    }
}

renderjobsToDom();

// filterSelection("all")
function filterSelection(myJobs) {
  // console.log(myJobs);
  if (myJobs == "all"){
    renderjobsToDom()
  } else {
    renderjobsToDom(myJobs);
  }
}


// Get the modal login button
var modal = document.getElementById('modal-button');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// Below function Executes on click of login button.
let login = document.forms['login'];

login.addEventListener('submit', loginUser);

let adminEmail = "admin@email.com";
let adminPassword = "admin123";

function loginUser(e) {
    e.preventDefault();
    const email = login.username.value.trim();
    const password = login.psw.value.trim();
    console.log(email, password);
    if (email == adminEmail && password == adminPassword) {
        window.location.href = 'public/www/admin.html'
    }
    else {
        window.location.href = '/'
    }
}





